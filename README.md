# starwars-dev-env

starwars service


## Getting started

To SetUp project in dev-env folder

```
./setup.sh
```

To Run app in dev-env folder

```
docker-compose up
```

To Run tests in dev-env folder

```
docker-compose run backend ./docker/run-tests.sh
```

## UI Documentation

- localhost:5000 -> Homepage, detail page after link click
- after download reload page to see updated list

![img_1.png](img_1.png)

- localhost:5000/:id -> Detailpage

![img_2.png](img_2.png)



## API Documentation:

- localhost:5000/swagger

![img.png](img.png)

## Improvements

- Add more test only most basic cases implemented  
- Add service to interact with CSV files,
  currently this responsibility is splitted between few classes
  
- Add cache for caching responses from SWAPI
    
