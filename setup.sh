#!/bin/bash
set -e

source .repos.sh

for repo in ${REPOS[*]}; do
  if [ ! -d "$repo" ]; then
    git clone "git@gitlab.com:starwarsapi/${repo}.git" $repo
    git -C $repo checkout main 
  else
    echo "${repo} already exists. Skipping."
  fi
done
